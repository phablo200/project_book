@php 
  $background  = asset("loginin/background_1.jpg");
@endphp  
<!DOCTYPE html>
<html lang="pt-br">
    <head> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">

        <!-- Website CSS style -->
        <link rel="stylesheet" type="text/css" href="{{asset('loginin/login.css')}}">

        <!-- Website Font style -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        
        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

        <title>NOME PROJETO</title>
        <style type="text/css">
            
        body {
            background: url(<?= $background ?>);
            background-size: 100%;            
        }

        </style>
    </head>


    <body>
        <div class="container">
            <div class="row main">
                <div class="main-login main-center">
                    @if (session('message'))
                        <div class="alert alert-danger">
                            {{ session('message') }}
                        </div>
                    @endif
                    <form class="form-horizontal" method="post" action="{{route('usuario.novo-login')}}">
                        {{csrf_field()}}
                        <div class="form-group {{ $errors->has('login') ? 'has-error' : ''}}">
                            <label for="login" class="cols-sm-2 control-label">LOGIN</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="login" id="login"  placeholder="Entre com seu login"/>
                                </div>
                            </div>
                            @if($errors->has('login'))
                                <span class="help-block">
                                    <strong style="color: red; font-size: 12px;"> {{$errors->first('login')}} </strong>
                                </span>
                            @endif

                        </div>

                        <div class="form-group {{ $errors->has('senha') ? 'has-error' : ''}}">
                            <label for="senha" class="cols-sm-2 control-label">SENHA</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="senha" id="senha"  placeholder="Entre com sua senha"/>
                                </div>
                            </div>
                            @if($errors->has('senha'))
                                <span class="help-block">
                                    <strong style="color: red; font-size: 12px;"> {{$errors->first('login')}} </strong>
                                </span>
                            @endif
                        </div>


                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary btn-lg btn-block login-button">
                                <i class="fa fa-sign-in"></i>   
                                LOGIN
                            </button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script>
    </body>
</html>

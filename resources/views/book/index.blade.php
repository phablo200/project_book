<!-- CRIADO POR PHABLO VILAS BOAS -->
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <title> NOME PROJETO </title>
    <meta name="viewport" content="width = device-width, minimum-scale=1, maximum-scale=1, user-scalable = no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- DEPENDENCIA JQuery -->
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- DEPENDENCIAS EXTERNAS JAVASCRIPT -->
    <script type="text/javascript" src="{{asset('assets/js/underscore-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/backbone-min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/jquery-confirm.css')}}">
    <!-- BIBLIOTECA Turn.js -->
    <script type="text/javascript" src="{{asset('assets/js/turn.min.js')}}"></script>
    
    <!-- DEPENDENCIAS DO APLICATIVO -->
    <script type="text/javascript" src="{{asset('assets/js/appflipbook.js')}}"></script>


     <!-- DEPENDENCIAS EXTERNAS CSS-->
<!--     <link rel="icon" type="image/png" href="assets/img/favicon.png?a" />
 -->    <link href='http://fonts.googleapis.com/css?family=Carrois+Gothic+SC' rel='stylesheet' type='text/css'>

    <!-- DEPENDENCIAS CSS -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/main.css')}}"></link>
    <script src="https://use.fontawesome.com/cf4f23372d.js"></script>
    <!-- MD BOOTSTRAP -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/css/mdb.min.css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/js/mdb.min.js">
    </script>
    <link rel="stylesheet" href="{{asset('css/main.css') }}"/>


    <style type="text/css">
      .jconfirm-box-container {
          width: 20%;
          position: absolute;
          left: 43%;
          top: 0%;
          text-align: center;
      }

    </style>
  </head>
<body>
  <!-- DEFININDO BACKGROUND O BOOK -->

  @php 
    if (count($imagens) > 0) { 
      $background  = asset("assets/images/" . $usuario->usu_id . "/" . $imagens[0]->boo_id . "/" . $background);
    }
  @endphp
  <div class="catalog-app" @if (count($imagens) > 0) style='background: url("<?= $background ?>")' >
        <button class="bt-icon-top" id="logout"><i class="fa fa-sign-out" aria-hidden="true"></i></button>
        <button class="bt-icon-top" id="playMusic"><i class="fa fa-music"></i></button>
    <div id="loading-gif">
      <img src="{{asset('images/loader-2.gif')}}" />
    </div>
    
    <div id="viewer" style="display:none;">
    <div id="flipbook" class="ui-flipbook">
        @if (Session('message'))
          <div class="alert alert-danger">
            {{ Session('message') }}
          </div>
        @endif
        @for($i = 0; $i<count($imagens); $i++)
          @php
            $path = asset("assets/images/" . $usuario->usu_id . "/" . $imagens[$i]->boo_id . "/" . $imagens[$i]->ima_indice . "." . $imagens[$i]->ima_extensao);
          @endphp
          @if ($i == 0)
            <div>
              <div id="firstlink" style="text-align: center;  margin: auto;position: absolute; top: 0; left: 0; bottom: 0; right: 0;">
                <img src="<?= $path ?>" width="100%" height="100%" class="s1" />
              </div>
            </div>
          @else
            <div>
              <img src="<?= $path?>" width="100%" height="100%" />
            </div>
          @endif
        @endfor
      <a ignore="1" class="ui-arrow-control ui-arrow-next-page"></a>
      <a ignore="1" class="ui-arrow-control ui-arrow-previous-page"></a>
    </div>
  </div>
  <!-- class="ui-arrow-control ui-arrow-previous-page ui-arrow-control-tap -->

  <!-- controls -->
  <div id="controls">
    <div class="all">
        <div class="ui-slider" id="page-slider">
        <div class="bar">
          <div class="progress-width">
            <div class="progress">
              <div class="handler"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="ui-options" id="options">
        <a class="ui-icon" id="ui-icon-table-contents">
          <i class="fa fa-bars"></i>
        </a>
        <a class="ui-icon show-hint" title="Miniatures" id="ui-icon-miniature">
          <i class="fa fa-th"></i>
        </a>
        <a class="ui-icon" id="ui-icon-zoom">
          <i class="fa fa-file-o"></i>
        </a>
        <a class="ui-icon show-hint" title="Share" id="ui-icon-share">
          <i class="fa fa-share"></i>
        </a>
        <a class="ui-icon show-hint" title="Full Screen" id="ui-icon-full-screen">
          <i class="fa fa-expand"></i>
        </a>
        <a class="ui-icon show-hint" id="ui-icon-toggle">
          <i class="fa fa-ellipsis-h"></i>
        </a>
      </div>

      <!-- zoom slider -->
      <div id="zoom-slider-view" class="zoom-slider">
          <div class="bg">
             <div class="ui-slider" id="zoom-slider">
              <div class="bar">
                <div class="progress-width">
                  <div class="progress">
                    <div class="handler"></div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <!-- / zoom slider -->
    </div>
    
    <div id="ui-icon-expand-options">
      <a class="ui-icon show-hint">
        <i class="fa fa-ellipsis-h"></i>
      </a>
    </div>

  </div>
  <!-- / controls -->

  <!-- miniatures -->
  <div id="miniatures" class="ui-miniatures-slider">    

        
  </div>
  <!-- / miniatures -->

  <!-- FORMUlÁRIO LOGOUT -->
  <form action="{{route('usuario.logout')}}" method="POST" id="formLogout">
    {{csrf_field()}}
  </form>
  <!-- FIM FORMUlÁRIO -->

  <script type="text/javascript" src="{{asset('js/jquery-confirm.js')}}"></script>
  @else
    <h3>OPS, Não encontramos nenhuma imagem sua !</h3>
  @endif

  @if (count($imagens) > 0)
    <script >
      FlipbookSettings = {
      options: {
        width: 1280,
        height: 920
      },

      shareMessage: 'Introducing turn.js 5 - HTML5 Library for Flipbooks.',

      table: [
        {text: 'Cover', page: 1},
        {text: 'Introduction', page: 2},
        {text: 'Getting Started', page: 4},
        {text: 'Reference', page: 6},
        {text: 'Pre-order', page: 8},
        {text: 'About', page: 12}
      ],

      pageFolder: "{{asset('assets/images') . '/' .  $usuario->usu_id . '/' . $imagens[0]->boo_id . '/' }}"

    };

    $('.ui-arrow-next-page').click(function (){
      $("#flipbook").turn("next");
    })

    $('.ui-arrow-previous-page').click( function () {
      $("#flipbook").turn("previous");
    });


    $(window).load(function(event) {
        $('#loading-gif').attr("style", "display:none");
        $('#viewer').removeAttr("style");

       var audio = new Audio("{{asset('assets/audio/background_music.mp3')}}");
       audio.loop = true;
       audio.play();
       $('#playMusic').click(function () {
          if (audio.paused) {
             audio.play();
          } else {
             audio.pause();
          } 
        });
         $("#flipbook .page-1").addClass('animation-on');
         $('.jconfirm-content').click(function () {
            a
         });
     });

    $('#logout').click(function (e) {
            $.dialog({
                title: "<img src={{asset('loginin/atemais.png')}} width='20%'>",
                content: "ATÉ MAIS, VOLTE SEMPRE !",
            });
            
            setTimeout(function () {
                $('#formLogout').submit();
            }, 3000);  
            
      });

    </script>
    <script type="text/javascript" src="{{asset('js/logout.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery-confirm.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/logout.js')}}"></script>
  @endif
  
  </body>
</html>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get("/", "InicioController@index")->name("inicio.index");
Route::get("/login",  "InicioController@login")->name("login");
Route::post("/efetuar-login", "Usuario\UsuarioController@novoLogin")->name("usuario.novo-login");
Route::any("/logout", "Usuario\UsuarioController@logout")->name("usuario.logout");





Route::get("/administracao", "Administracao/AdministracaoControlelr@index")->name("administracao.index");
Route::get("/administracao/cliente", "Administracao/AdministracaoControlelr@clientes")->name("administracao.clientes");


Route::get("/book", "Cliente\ClienteController@index")->name("cliente.index");

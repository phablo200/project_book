
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 15/08/2017 às 10:55:46
-- Versão do Servidor: 10.1.24-MariaDB
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `u778394827_phbd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bc_book`
--

CREATE TABLE IF NOT EXISTS `bc_book` (
  `boo_id` int(11) NOT NULL AUTO_INCREMENT,
  `boo_descricao` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `boo_audio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boo_background` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boo_data_atualizacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `boo_data_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `boo_usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`boo_id`),
  UNIQUE KEY `book_UNIQUE` (`boo_id`),
  KEY `fk_bc_book_bc_usuario1_idx` (`boo_usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `bc_book`
--

INSERT INTO `bc_book` (`boo_id`, `boo_descricao`, `boo_audio`, `boo_background`, `boo_data_atualizacao`, `boo_data_cadastro`, `boo_usuario_id`) VALUES
(1, 'book_exemplo', '1', 'jpg', '2017-08-14 02:38:25', '2017-08-14 02:38:25', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `bc_imagem`
--

CREATE TABLE IF NOT EXISTS `bc_imagem` (
  `ima_id` int(11) NOT NULL AUTO_INCREMENT,
  `ima_descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ima_extensao` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `ima_data_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ima_data_atualizacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ima_book_id` int(11) NOT NULL,
  `ima_indice`  int(11) NOT NULL,
  PRIMARY KEY (`ima_id`),
  UNIQUE KEY `idbc_imagem_UNIQUE` (`ima_id`),
  KEY `fk_bc_imagem_bc_book1_idx` (`ima_book_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `bc_imagem`
--

INSERT INTO `bc_imagem` (`ima_id`, `ima_descricao`, `ima_extensao`, `ima_data_cadastro`, `ima_data_atualizacao`, `ima_book_id`, `ima_indice`) VALUES
(1, 'descricao_exemplo', 'jpg', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 1),
(2, 'descricao_exemplo', 'jpg', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 2),
(3, 'descricao_exemplo', 'jpg', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 3),
(4, 'descricao_exemplo', 'JPG', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 4),
(5, 'descricao_exemplo', 'jpg', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 5),
(6, 'descricao_exemplo', 'JPG', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 6),
(7, 'descricao_exemplo', 'JPG', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 7),
(8, 'descricao_exemplo', 'JPG', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 8),
(9, 'descricao_exemplo', 'jpg', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 9),
(10, 'descricao_exemplo', 'JPG', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 10),
(11, 'descricao_exemplo', 'jpg', '2017-08-14 02:39:01', '2017-08-14 02:39:01', 1, 11),
(12, 'descricao', 'jpg', '2017-08-14 02:48:19', '2017-08-14 02:48:19', 1, 12);

-- --------------------------------------------------------

--
-- Estrutura da tabela `bc_tipo_usuario`
--

CREATE TABLE IF NOT EXISTS `bc_tipo_usuario` (
  `tiu_id` int(11) NOT NULL AUTO_INCREMENT,
  `tiu_descricao` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tiu_data_atualizacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tiu_data_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tiu_id`),
  UNIQUE KEY `idbc_tipo_usuario_UNIQUE` (`tiu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `bc_tipo_usuario`
--

INSERT INTO `bc_tipo_usuario` (`tiu_id`, `tiu_descricao`, `tiu_data_atualizacao`, `tiu_data_cadastro`) VALUES
(1, 'ADMINISTRADOR', '2017-08-14 02:31:15', '2017-08-14 02:31:15'),
(2, 'CLIENTE', '2017-08-14 02:31:15', '2017-08-14 02:31:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bc_usuario`
--

CREATE TABLE IF NOT EXISTS `bc_usuario` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usu_referencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usu_login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usu_senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usu_data_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usu_data_atualizacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usu_tipo_usuario_id` int(11) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usu_id`),
  UNIQUE KEY `usu_id_UNIQUE` (`usu_id`),
  KEY `fk_bc_usuario_bc_tipo_usuario_idx` (`usu_tipo_usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `bc_usuario`
--

INSERT INTO `bc_usuario` (`usu_id`, `usu_nome`, `usu_referencia`, `usu_login`, `usu_senha`, `usu_data_cadastro`, `usu_data_atualizacao`, `usu_tipo_usuario_id`, `remember_token`) VALUES
(2, '100958', '100958', '100958', '$2y$10$Q4dLGWzdTZBSJIf2t0pEquYRQTAqa4ZAgMG5d6z3CUjEcxBUAT1Dm', '2017-08-14 02:32:47', '2017-08-14 02:32:47', 2, '1slZbLCt5vlrK2z020becg81CmeoZjucreH0Yc2J4w3lDksV5Nv9YolKNDKd');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

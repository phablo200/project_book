<?php

namespace App\Http\Controllers\Usuario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Usuario\LoginRequest;
use App\Models\Usuario;
use Hash, Auth, Session;

class UsuarioController extends Controller
{
	protected $usu;

	public function __construct()
	{
		$this->usu = new Usuario();
	}

    public function novoLogin(LoginRequest $request)
    {

    	$usuario = $this->usu->where("usu_login", "=", trim($request->login))->first();

    	if (count($usuario) == 0 || is_null($usuario))  {
    		Session::flash('message', "Login ou senha incorreta");
    		return redirect()->back();
    	} else {
    		if ($usuario && Hash::check($request->senha, $usuario->usu_senha)) {
    			// LOGADO
    			Auth::login($usuario);
    			return redirect()->route('cliente.index');    			
    		} else {
    			Session::flash('message', "Login ou senha incorreta");
    			return redirect()->back();
    		}

    	}
    }


    public function logout(Request $request) 
    {
    	Auth::logout();
    	return redirect()->route("login");

    }
}

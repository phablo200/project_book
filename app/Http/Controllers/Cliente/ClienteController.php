<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Usuario;
use Auth;

class ClienteController extends Controller
{

	protected $usu;

	public function __construct()
	{
		$this->middleware('auth');
		$this->usu = new Usuario();
	}

    public function index()
    {

    	$imagens = $this->usu
    		->join("bc_book", "bc_book.boo_usuario_id", "=", "bc_usuario.usu_id")
    		->join("bc_imagem", "bc_book.boo_id", "=", "bc_imagem.ima_book_id")
    		->select("bc_imagem.ima_id", "bc_imagem.ima_indice", "bc_book.boo_id",
    				 "bc_book.boo_background", "bc_imagem.ima_id", "bc_imagem.ima_extensao"
    				)
    		->where("bc_usuario.usu_id", "=", Auth::user()->usu_id)
    		->orderBy("bc_imagem.ima_indice", "ASC")
    		->get();

    	$usuario = Auth::user();
		if (count($imagens) > 0) { 	
    		$background = "background_" . $imagens[0]->boo_id . "." . $imagens[0]->boo_background;
			return view("book.index", compact("imagens", "usuario", "background"));
		} else {
			return view("book.index", compact("imagens", "usuario"));
		}    	
    }
}

<?php

namespace App\Http\Middleware\Autenticar;

use Closure;
use App\Models\Usuario;
use Auth;

class Middleware
{

    protected $usu;

    public function __construct()
    {
        $this->usu = new Usuario();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user()->usu_tipo_usuario_id == 2) {
            // FUTURO
        }
        return $next($request);
    }
}

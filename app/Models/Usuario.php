<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;



class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = "bc_usuario";
    protected $primaryKey = "usu_id";
    protected $fillable = ["usu_nome", "usu_login", "usu_senha", "usu_referencia", "usu_data_cadastro",
    						"usu_data_atualizacao" ];

    protected $hidden = ["usu_senha", "remember_token"];						


    public $timestamp = true;
    const CREATED_AT = "usu_data_cadastro";
    const UPDATED_AT = "usu_data_atualizacao";						

}

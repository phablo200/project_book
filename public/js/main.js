FlipbookSettings = {
    options: {
      width: 1280,
      height: 920
    },

    shareMessage: 'Introducing turn.js 5 - HTML5 Library for Flipbooks.',

    table: [
      {text: 'Cover', page: 1},
      {text: 'Introduction', page: 2},
      {text: 'Getting Started', page: 4},
      {text: 'Reference', page: 6},
      {text: 'Pre-order', page: 8},
      {text: 'About', page: 12}
    ],

    pageFolder: "{{asset('assets/images') . '/' .  $usuario->usu_id}}"

  };

  $('.ui-arrow-next-page').click(function (){
    $("#flipbook").turn("next");
  })

  $('.ui-arrow-previous-page').click( function () {
    $("#flipbook").turn("previous");
  });

  //        $('.magazine').turn('next');

    $(window).load(function(event) {  
       var audio = new Audio("{{asset('assets/audio/background_music.mp3')}}");
       audio.loop = true;
       audio.play();
       $('#playMusic').click(function () {
          if (audio.paused) {
             audio.play();
          } else {
              audio.pause();
          } 
       });

       $("#flipbook .page-1").addClass('animation-on');
    }); 


    count = 0;
    $('#ui-icon-full-screen').click(function () {
        
    });